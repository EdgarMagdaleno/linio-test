#!/bin/bash

for f in tests/*; do
	RANGE="$(cut -d "/" -f 2 <<< $f)"
	echo -n "Test [$RANGE]: "
	EXE_OUT=$(java Linio $RANGE)
	TEST_OUT=$(cat "$f")

	if [ "$EXE_OUT" == "$TEST_OUT" ]
	then
		echo PASSED
	else
		echo FAILED
	fi
done
