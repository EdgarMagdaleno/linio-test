import java.util.ArrayList;

/* Transforms a certain input into a certain output by applying a rule. */
abstract class Rule < I, O > {
	abstract public boolean rule_applies(I i);
	abstract O get_output(I i);
}

/*
	Implements the rule by calculating if a given number is divisible by the
	factors received in the constructor.
*/
abstract class FactorRule < T > extends Rule < Integer, T > {
	private int[] factors;

	FactorRule(int factor) {
		this.factors = new int[1];
		this.factors[0] = factor;
	}

	FactorRule(int[] factors) {
		this.factors = factors;
	}

	@Override
	public boolean rule_applies(Integer n) {
		int rem = 0;
		
		for (int factor : this.factors) {
			rem += n % factor;
		};

		return rem == 0;
	}
}

/* Simple generic iterator */
abstract class Iterator < T > {
	abstract boolean has_next();
	abstract T next();
}

/*
	Receives a lower and upper bound and iterates 1 by 1 by calling next().
*/
class NumberIterator extends Iterator < Integer > {
	private int lower_bound;
	private int upper_bound;

	NumberIterator(int lower_bound, int upper_bound) {
		this.lower_bound = lower_bound;
		this.upper_bound = upper_bound;
	}

	@Override
	public boolean has_next() {
		return lower_bound <= upper_bound;
	}

	@Override
	public Integer next() {
		return lower_bound++;
	}
}

/*
	Receives an iterator and a set of rules and applies them to
	each step of the iteration.
*/
abstract class RuleApplier < I > {
	protected ArrayList<? extends Rule> rules;
	protected Iterator<I> iterator;

	abstract public void start();
	abstract void step(I i);
}

/*
	Applies FactorRules to a NumberIterator.
*/
class FactorRuleApplier extends RuleApplier < Integer > {
	FactorRuleApplier(ArrayList<FactorRule<?>> rules, NumberIterator iterator) {
		this.rules = rules;
		this.iterator = iterator;

		/*
			If its a factor of 1 (fallback rule), output itself. This will
			only trigger if no other rule could be applied.
		*/
		rules.add(0, new FactorRule<Integer>(1) {
			@Override
			public Integer get_output(Integer n) {
				return n;
			}
		});
	}

	@Override
	public void start() {
		while (iterator.has_next()) {
			step(iterator.next());
		}
	}

	@Override
	public void step(Integer n) {
		/*
			Get the last rule of the list that can be applied to
			the current iteration number.
		*/
		int highest_rule = 0;

		for (int i = 0; i < rules.size(); i++) {
			if (rules.get(i).rule_applies(n)) {
				highest_rule = i;
			}
		}

		System.out.println(rules.get(highest_rule).get_output(n));
	}
}

class Linio {
	public static void main(String[] args) {
		/* Traverses a range of numbers */
		NumberIterator iterator = new NumberIterator(Integer.parseInt(args[0]),
													 Integer.parseInt(args[1]));

		/* Set of rules to be applied each step of the iteration */
		ArrayList<FactorRule<?>> rules = new ArrayList<FactorRule<?>>();

		/* If its a factor of 3, output Linio. */
		rules.add(new FactorRule<String>(3) {
			@Override
			public String get_output(Integer n) {
				return "Linio";
			}
		});

		rules.add(new FactorRule<String>(5) {
			@Override
			public String get_output(Integer n) {
				return "IT";
			}
		});

		/* To check multiple factors are defined with arrays is used. */
		rules.add(new FactorRule<String>(new int[] { 3, 5 }) {
			@Override
			public String get_output(Integer n) {
				return "Linioas";
			}
		});

		/*
			Applies the set of rules to each step of the iterator and
			outputs the result.
		*/
		FactorRuleApplier applier = new FactorRuleApplier(rules, iterator);
		applier.start();
	}
}
